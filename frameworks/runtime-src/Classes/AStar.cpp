#include "AStar.h"
using namespace astar ;

Point::Point()
{
	_x = _y = 0;
}
Point::Point(int x, int y)
{
	_x = x;
	_y = y;
}
int Point::getManhattanDistance(const Point& t) const
{
	return std::abs(_x - t._x) + std::abs(_y - t._y);
}
double Point::getEuclidDistance(const Point& t) const
{
	return std::sqrt((_x - t._x) * (_x - t._x) + (_y - t._y) * (_y - t._y));
}
bool Point::operator== (const Point& t) const
{
	return _x == t._x && _y == t._y;
}
bool Point::operator< (const Point& t) const
{
	if (_x == t._x)
	{
		return _y < t._y;
	}
	return _x < t._x;
}

Local::Local() : Point()
{
	_g = _h = 0;
}
Local::Local(int x, int y, int g, int h) : Point(x, y)
{
	_g = g;
	_h = h;
}
Local::Local(const Point& t, int g, int h) : Point(t)
{
	_g = g;
	_h = h;
}
Local::Local(const Local& t)
{
	_x = t._x;
	_y = t._y;
	_g = t._g;
	_h = t._h;
}
int Local::getF() const
{
	return _g + _h;
}
bool Local::operator< (const Local& t) const
{
	return getF() < t.getF();
}

CloseList::CloseList()
{
	newClose(100, 100);
}
CloseList::CloseList(int x, int y)
{
	newClose(x, y);
}
CloseList::~CloseList()
{
	for (int i = 0; i < size_y; i++)
	{
		delete[] _close[i];
	}
	delete[] _close;
}
void CloseList::insert(const Local& p)
{
	_close[p.getX()][p.getY()] = true;
}
bool CloseList::find(const Local& p) const
{
	return _close[p.getX()][p.getY()] == true;
}
void CloseList::newClose(int x, int y)
{
	size_x = x;
	size_y = y;
	_close = new bool*[x];
	for (int i = 0; i < x; i++)
	{
		_close[i] = new bool[y];
		memset(_close[i], false, sizeof(_close[i]));
	}
}

AStar::AStar(std::vector<std::vector<bool> >& legal, Point& start, Point& end)
{
	_legal = legal;
	_start = start;
	_end = end;
	pathPlanning();
}
int AStar::getDistance() const
{
	return _distance;
}
std::list<Point> AStar::getPath()  const
{
	return _path_list;
}
void AStar::pathPlanning()
{
	open.insert(Local(_start, 0, 0));
	//8个方向
	int dir[][2] = {
		{ 0, 1 }, { 0, -1 },
		{ 1, 0 }, { -1, 0 },
		{ -1, 1 }, { 1, 1 },
		{ -1, -1 }, { 1,  -1}
	};
	while (!open.empty())
	{
		Local cur = *open.begin();
		open.erase(open.begin());
		close.insert(cur);
		if (cur == _end)
		{
			initPath();
			_distance = cur.getG();
			return;
		}
		for (int i = 0; i < 8; i++)
		{
			int x = cur.getX() + dir[i][0];
			int y = cur.getY() + dir[i][1];
			int g = cur.getG() + 1;
			int h = _end.getManhattanDistance(Point(x, y));
			Local point_choose(x, y, g, h);
			checkLocal(point_choose, cur);
		}
	}
	_distance = -1;
}
void AStar::checkLocal(Local& local_tar, Local& local_cur)
{
	int x = local_tar.getX();
	int y = local_tar.getY();
	int g = local_tar.getG();
	int h = local_tar.getH();
	if (x < _legal.size() && x >= 0 && y < _legal[0].size() && y >= 0 &&
		_legal[x][y] && !close.find(local_tar))
	{
		bool inopen = false;
		for (std::multiset<Local>::iterator it = open.begin(); it != open.end(); it++)
		{
			if (it->getX() == x && it->getY() == y)
			{
				inopen = true;
				if (it->getG() > g)
				{
					open.erase(it);
					open.insert(local_tar);
					_path_pre[local_tar] = local_cur;
					break;
				}
			}
		}
		if (!inopen)
		{
			open.insert(local_tar);
			_path_pre[local_tar] = local_cur;
		}
	}
}
void AStar::initPath()
{
	Point path_res = _end;
	while (!(path_res == _start))
	{
		_path_list.push_front(path_res);
		path_res = _path_pre[path_res];
	}
	_path_list.push_front(_start);
}






//---------------------------------

LuaAstart::~LuaAstart()
{
	m_spPostion.clear();
	m_pathlist.clear();
	m_maps.clear();
}

bool LuaAstart::init(bool isShow, cocos2d::Node* parent, int boxWidth , int boxHeigh, std::string name)
{
	if (isShow)	CCAssert(parent != nullptr, "parent is null !");
	if (boxHeigh == 0 || boxWidth == 0)
	{
		return false;
	}

	m_boxWidth = boxWidth;
	m_boxHeigh = boxHeigh;
	m_boxName = name;

	//初始化地图
	initMap(parent);

	return true;
}
bool LuaAstart::caclPath(cocos2d::Point curPoint, cocos2d::Point tarPoint)
{
	//如果坐标相等就没必要运算了
	if (curPoint == tarPoint)
	{
		return true;
	}
	int startX = 0;
	int startY = 0;
	int endX = 0;
	int endY = 0;

	for (auto& data : m_spPostion)
	{
		if (data->getBoundingBox().containsPoint( curPoint ))
		{
			int  tag =  data->getTag();

			//装换成对应的坐标
			startX  = tag / 48;
			startY = data->getTag() - startX * 48 ;

			data->setColor(cocos2d::Color3B::BLACK);

			break;
		}
	}

	for (auto& data : m_spPostion)
	{
		if (data->getBoundingBox().containsPoint( tarPoint ))
		{
			int  tag =  data->getTag();

			//装换成对应的坐标
			endX  = tag / 48;
			endY = data->getTag() - endX * 48 ;

			data->setColor(cocos2d::Color3B::BLACK);

			break;
		}
	}

	astar::Point start(startX, startY);
	astar::Point end(endX, endY);

	astar::AStar* astars = new astar::AStar(m_maps, start, end);
	auto path = astars->getPath();
	m_pathlist.clear();
	for (auto& path_cur : path)
	{
		int tag = path_cur.getX()*48 + path_cur.getY();

		//从所有的格子中找到对应tag的格子，并保存它的坐标作为路径
		for (auto& data : m_spPostion)
		{
			auto tmp = static_cast<cocos2d::Sprite*>(data);
			if (tmp->getTag() == tag)
			{
				auto pos = tmp->getPosition();
				tmp->setColor(cocos2d::Color3B::BLUE);
				auto tmps = astar::MyVec2::create();
				tmps->setx( pos.x );
				tmps->sety( pos.y );

				m_pathlist.pushBack(tmps);
				break;

			}

		}

	}
		
	return true;
}

void LuaAstart::initMap(cocos2d::Node* parent)
{
	int x = 0;
	int y = 0;
	int z = 1;
	//地图初始化以 20*20 每块的大小，在 960*640的分辨率下
	std::vector<bool>maps;
	for (int i = 0; i <(int)(640 /  m_boxHeigh) ; ++ i)
	{
		maps.clear();
		int m = rand()%48;
		for (int j = 0; j < (int)(960 / m_boxWidth); j++)
		{
			auto box = cocos2d::Sprite::create(m_boxName.c_str());
			box->setPosition(cocos2d::Vec2(x,y));
			m_spPostion.pushBack( box );
			box->setTag(i*48 + j);
			
			if (parent != nullptr)
			{
				parent->addChild( box);
			}
			else
			{
				box->setVisible(false);
			}
			x += m_boxWidth + z;

			if (j == m)
			{
				maps.push_back(0);
				box->setColor(cocos2d::Color3B::RED);
			}
			else
			{
				maps.push_back(1);
			}

		}
		x = 0;
		y += m_boxHeigh + z ;
		m_maps.push_back(maps);
	}

}

bool LuaAstart::setbarrier(int line, int row)
{
	//如果索引为负数，或者地图未初始化则失败
	if (line == -1 || row == -1 || m_maps.empty())
	{
		return false;
	}

	//根据指定的索引，设置指定块为障碍物
	m_maps[line][row] = true;

	return true;

}

MyVec2::MyVec2()
{
	;
}
MyVec2::MyVec2(float x, float y) : x(x),y(y)
{
}

MyVec2::~MyVec2()
{
}
