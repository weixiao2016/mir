/***************************
*    A星寻路算法
*
*    Anti-Magic
*    2014/5/26
****************************/

#ifndef __ASTAR_H__
#define __ASTAR_H__

#include <map>
#include <set>
#include <vector>
#include <list>
#include "cocos2d.h"

namespace astar
{


//属性宏
#define SYNTHESIZE(varType, varName, funName) \
protected: \
	varType varName; \
public: \
	virtual varType get##funName(void) const { \
	return varName; \
} \
	virtual void set##funName(varType var) { \
	varName = var; \
}

/***************************
*    Point类
*    表示一个二维坐标
****************************/
class Point
{
	SYNTHESIZE(int, _x, X);
	SYNTHESIZE(int, _y, Y);
public:
	Point();
	Point(int x, int y);
	//获得曼哈顿距离
	int getManhattanDistance(const Point& t) const;
	//获得欧几里得距离
	double getEuclidDistance(const Point& t) const;
	bool operator== (const Point& t) const;
	bool operator< (const Point& t) const;
};

/***************************
*    Local类
*    在A星寻路算法中的单位区域
****************************/
class Local : public Point
{
	SYNTHESIZE(int, _g, G);
	SYNTHESIZE(int, _h, H);
public:
	Local();
	Local(int x, int y, int g, int h);
	Local(const Point& t, int g, int h);
	Local(const Local& t);
	int getF() const;
	bool operator< (const Local& t) const;
};

/***************************
*    SuperCloseList类 抽象类
*    Close列表的基类
*    因为实现Close列表在不同场合有不同的实现
*    需要继承此类
****************************/
class SuperCloseList
{
public:
	//插入close列表
	virtual void insert(const Local& p) = 0;
	//查询close列表中是否已存在此区域
	virtual bool find(const Local& p) const = 0;
};

/***************************
*    CloseList类
*    Close列表的哈希实现
****************************/
class CloseList : public SuperCloseList
{
public:
	CloseList();
	CloseList(int x, int y);
	~CloseList();
	void insert(const Local& p) override;
	bool find(const Local& p) const override;
private:
	bool** _close;
	int size_x, size_y;
	//申请数组内存
	void newClose(int x, int y);
};

/***************************
*    A星算法类
*    实现A星算法的逻辑
****************************/
class AStar
{
public:
	AStar(std::vector<std::vector<bool> >& legal, Point& start, Point& end);
	//获得距离
	int getDistance() const;
	//获得路径
	std::list<Point> getPath() const;
private:
	std::vector<std::vector<bool> > _legal;
	std::multiset<Local> open;
	CloseList close;
	Point _start;
	Point _end;
	std::map<Point, Point> _path_pre;
	std::list<Point> _path_list;
	int _distance;
	//路径规划，算法的核心
	void pathPlanning();
	//检查区域
	void checkLocal(Local& local_tar, Local& local_cur);
	//生成路径
	void initPath();
};


#define CREATE_LUASTARTFUNC(__TYPE__) \
	static __TYPE__* create(bool isShow, cocos2d::Node* parent,int boxWidth , int boxHeigh , std::string name) \
{ \
	__TYPE__ *pRet = new(std::nothrow) __TYPE__(); \
	if (pRet && pRet->init(isShow, parent, boxWidth, boxHeigh, name)) \
{ \
	pRet->autorelease(); \
	return pRet; \
} \
	else \
{ \
	delete pRet; \
	pRet = NULL; \
	return NULL; \
} \
}

class MyVec2 : public cocos2d::Ref
{
public:
	MyVec2();
	MyVec2(float x, float y);
	~MyVec2();

	CC_SYNTHESIZE(float, x, x);
	CC_SYNTHESIZE(float, y, y);

	bool init()
	{
		return true;
	}
	CREATE_FUNC(MyVec2);

private:

};


//用于转换LUA 使用C++的A星算法封装
class LuaAstart :  public cocos2d::Ref
{
public:
	LuaAstart()
	{
		;
	}

	~LuaAstart();

	//初始化
	//isShow : 是否显示格子
	//parent : 父节点
	//boxWidth , boxHeigh : 格子宽高
	//return true 表示成功
	bool init(bool isShow, cocos2d::Node* parent,  int boxWidth , int boxHeigh, std::string name);


	//构建函数
	CREATE_LUASTARTFUNC(LuaAstart);

	//获取整个路径表
	CC_SYNTHESIZE(cocos2d::Vector<astar::MyVec2*>, m_pathlist, PathList);

	//运算路径
	//curPoint: 起始坐标点
	//tarPoint: 终止坐标点
	//return true 表示成功
	bool caclPath(cocos2d::Point curPoint, cocos2d::Point tarPoint);

	//设置地图障碍物
	//line : 地图行索引 都从0开始
	//row ： 地图列索引
	//return true 表示成功
	bool  setbarrier(int line, int row);


	//地图格子
	CC_SYNTHESIZE(cocos2d::Vector<cocos2d::Sprite*>, m_spPostion, spPostion);

private:

	//初始化地图，以tilemap 方式
	//parent : 父节点用于现实格子
	//地图行数按块算 width
	//地图列数按块算 heigh
	//地图
	void initMap(cocos2d::Node* parent);

	//地图0为正常路，1为障碍物
	std::vector<std::vector<bool> > m_maps;

	//地图格子的宽高
	int m_boxWidth;
	int m_boxHeigh;

	//格子文件
	std::string m_boxName;




};


};
#endif
