
local MainScene = class("MainScene", cc.load("mvc").ViewBase)

MainScene.RESOURCE_FILENAME = "MainScene.csb"

local astart
local function OnTouch(touch ,event)

    local point = touch:getLocation()
    print(point.x, point.y)

    
    astart:caclPath(cc.p(0,0), point)
    local paths = astart:getPathList()
    
    for i = 1 , table.getn(paths) do
        print("x = " .. paths[i]:getx() .. "y = " .. paths[i]:gety() )
    end
    
end


function MainScene:onCreate()
    printf("resource node = %s", tostring(self:getResourceNode()))
    
    astart = as.LuaAstart:create(false, NULL, 20, 20, "test.png")
    astart:retain()
   
    local list = cc.EventListenerTouchOneByOne:create()
    list:registerScriptHandler(OnTouch, cc.Handler.EVENT_TOUCH_BEGAN)
    cc.Director:getInstance():getEventDispatcher():addEventListenerWithSceneGraphPriority(list,self)
    
end

return MainScene
