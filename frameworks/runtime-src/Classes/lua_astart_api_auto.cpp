#include "lua_astart_api_auto.hpp"
#include "AStar.h"
#include "tolua_fix.h"
#include "LuaBasicConversions.h"



int lua_astart_MyVec2_getx(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::MyVec2*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_MyVec2_getx'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_getx'", nullptr);
            return 0;
        }
        double ret = cobj->getx();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.MyVec2:getx",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_getx'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_MyVec2_gety(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::MyVec2*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_MyVec2_gety'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_gety'", nullptr);
            return 0;
        }
        double ret = cobj->gety();
        tolua_pushnumber(tolua_S,(lua_Number)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.MyVec2:gety",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_gety'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_MyVec2_init(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::MyVec2*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_MyVec2_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init();
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.MyVec2:init",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_init'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_MyVec2_setx(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::MyVec2*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_MyVec2_setx'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "as.MyVec2:setx");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_setx'", nullptr);
            return 0;
        }
        cobj->setx(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.MyVec2:setx",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_setx'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_MyVec2_sety(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::MyVec2*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_MyVec2_sety'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        double arg0;

        ok &= luaval_to_number(tolua_S, 2,&arg0, "as.MyVec2:sety");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_sety'", nullptr);
            return 0;
        }
        cobj->sety(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.MyVec2:sety",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_sety'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_MyVec2_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"as.MyVec2",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 0)
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_MyVec2_create'", nullptr);
            return 0;
        }
        astar::MyVec2* ret = astar::MyVec2::create();
        object_to_luaval<astar::MyVec2>(tolua_S, "as.MyVec2",(astar::MyVec2*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "as.MyVec2:create",argc, 0);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_create'.",&tolua_err);
#endif
    return 0;
}
int lua_astart_MyVec2_constructor(lua_State* tolua_S)
{
    int argc = 0;
    astar::MyVec2* cobj = nullptr;
    bool ok  = true;
#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

    argc = lua_gettop(tolua_S)-1;
    do{
        if (argc == 2) {
            double arg0;
            ok &= luaval_to_number(tolua_S, 2,&arg0, "as.MyVec2:MyVec2");

            if (!ok) { break; }
            double arg1;
            ok &= luaval_to_number(tolua_S, 3,&arg1, "as.MyVec2:MyVec2");

            if (!ok) { break; }
            cobj = new astar::MyVec2(arg0, arg1);
            cobj->autorelease();
            int ID =  (int)cobj->_ID ;
            int* luaID =  &cobj->_luaID ;
            toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"as.MyVec2");
            return 1;
        }
    }while(0);
    ok  = true;
    do{
        if (argc == 0) {
            cobj = new astar::MyVec2();
            cobj->autorelease();
            int ID =  (int)cobj->_ID ;
            int* luaID =  &cobj->_luaID ;
            toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"as.MyVec2");
            return 1;
        }
    }while(0);
    ok  = true;
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n",  "as.MyVec2:MyVec2",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_astart_MyVec2_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_astart_MyVec2_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (MyVec2)");
    return 0;
}

int lua_register_astart_MyVec2(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"as.MyVec2");
    tolua_cclass(tolua_S,"MyVec2","as.MyVec2","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"MyVec2");
        tolua_function(tolua_S,"new",lua_astart_MyVec2_constructor);
        tolua_function(tolua_S,"getx",lua_astart_MyVec2_getx);
        tolua_function(tolua_S,"gety",lua_astart_MyVec2_gety);
        tolua_function(tolua_S,"init",lua_astart_MyVec2_init);
        tolua_function(tolua_S,"setx",lua_astart_MyVec2_setx);
        tolua_function(tolua_S,"sety",lua_astart_MyVec2_sety);
        tolua_function(tolua_S,"create", lua_astart_MyVec2_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(astar::MyVec2).name();
    g_luaType[typeName] = "as.MyVec2";
    g_typeCast["MyVec2"] = "as.MyVec2";
    return 1;
}

int lua_astart_LuaAstart_setbarrier(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_setbarrier'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        int arg0;
        int arg1;

        ok &= luaval_to_int32(tolua_S, 2,(int *)&arg0, "as.LuaAstart:setbarrier");

        ok &= luaval_to_int32(tolua_S, 3,(int *)&arg1, "as.LuaAstart:setbarrier");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_setbarrier'", nullptr);
            return 0;
        }
        bool ret = cobj->setbarrier(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:setbarrier",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_setbarrier'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_setPathList(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_setPathList'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vector<astar::MyVec2 *> arg0;

        ok &= luaval_to_ccvector(tolua_S, 2, &arg0, "as.LuaAstart:setPathList");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_setPathList'", nullptr);
            return 0;
        }
        cobj->setPathList(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:setPathList",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_setPathList'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_init(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_init'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 5) 
    {
        bool arg0;
        cocos2d::Node* arg1;
        int arg2;
        int arg3;
        std::string arg4;

        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "as.LuaAstart:init");

        ok &= luaval_to_object<cocos2d::Node>(tolua_S, 3, "cc.Node",&arg1);

        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "as.LuaAstart:init");

        ok &= luaval_to_int32(tolua_S, 5,(int *)&arg3, "as.LuaAstart:init");

        ok &= luaval_to_std_string(tolua_S, 6,&arg4, "as.LuaAstart:init");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_init'", nullptr);
            return 0;
        }
        bool ret = cobj->init(arg0, arg1, arg2, arg3, arg4);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:init",argc, 5);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_init'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_caclPath(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_caclPath'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 2) 
    {
        cocos2d::Point arg0;
        cocos2d::Point arg1;

        ok &= luaval_to_point(tolua_S, 2, &arg0, "as.LuaAstart:caclPath");

        ok &= luaval_to_point(tolua_S, 3, &arg1, "as.LuaAstart:caclPath");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_caclPath'", nullptr);
            return 0;
        }
        bool ret = cobj->caclPath(arg0, arg1);
        tolua_pushboolean(tolua_S,(bool)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:caclPath",argc, 2);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_caclPath'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_getPathList(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_getPathList'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_getPathList'", nullptr);
            return 0;
        }
        cocos2d::Vector<astar::MyVec2 *> ret = cobj->getPathList();
        ccvector_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:getPathList",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_getPathList'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_setspPostion(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_setspPostion'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 1) 
    {
        cocos2d::Vector<cocos2d::Sprite *> arg0;

        ok &= luaval_to_ccvector(tolua_S, 2, &arg0, "as.LuaAstart:setspPostion");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_setspPostion'", nullptr);
            return 0;
        }
        cobj->setspPostion(arg0);
        lua_settop(tolua_S, 1);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:setspPostion",argc, 1);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_setspPostion'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_getspPostion(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif


#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertype(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    cobj = (astar::LuaAstart*)tolua_tousertype(tolua_S,1,0);

#if COCOS2D_DEBUG >= 1
    if (!cobj) 
    {
        tolua_error(tolua_S,"invalid 'cobj' in function 'lua_astart_LuaAstart_getspPostion'", nullptr);
        return 0;
    }
#endif

    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_getspPostion'", nullptr);
            return 0;
        }
        cocos2d::Vector<cocos2d::Sprite *> ret = cobj->getspPostion();
        ccvector_to_luaval(tolua_S, ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:getspPostion",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_getspPostion'.",&tolua_err);
#endif

    return 0;
}
int lua_astart_LuaAstart_create(lua_State* tolua_S)
{
    int argc = 0;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif

#if COCOS2D_DEBUG >= 1
    if (!tolua_isusertable(tolua_S,1,"as.LuaAstart",0,&tolua_err)) goto tolua_lerror;
#endif

    argc = lua_gettop(tolua_S) - 1;

    if (argc == 5)
    {
        bool arg0;
        cocos2d::Node* arg1;
        int arg2;
        int arg3;
        std::string arg4;
        ok &= luaval_to_boolean(tolua_S, 2,&arg0, "as.LuaAstart:create");
        ok &= luaval_to_object<cocos2d::Node>(tolua_S, 3, "cc.Node",&arg1);
        ok &= luaval_to_int32(tolua_S, 4,(int *)&arg2, "as.LuaAstart:create");
        ok &= luaval_to_int32(tolua_S, 5,(int *)&arg3, "as.LuaAstart:create");
        ok &= luaval_to_std_string(tolua_S, 6,&arg4, "as.LuaAstart:create");
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_create'", nullptr);
            return 0;
        }
        astar::LuaAstart* ret = astar::LuaAstart::create(arg0, arg1, arg2, arg3, arg4);
        object_to_luaval<astar::LuaAstart>(tolua_S, "as.LuaAstart",(astar::LuaAstart*)ret);
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d\n ", "as.LuaAstart:create",argc, 5);
    return 0;
#if COCOS2D_DEBUG >= 1
    tolua_lerror:
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_create'.",&tolua_err);
#endif
    return 0;
}
int lua_astart_LuaAstart_constructor(lua_State* tolua_S)
{
    int argc = 0;
    astar::LuaAstart* cobj = nullptr;
    bool ok  = true;

#if COCOS2D_DEBUG >= 1
    tolua_Error tolua_err;
#endif



    argc = lua_gettop(tolua_S)-1;
    if (argc == 0) 
    {
        if(!ok)
        {
            tolua_error(tolua_S,"invalid arguments in function 'lua_astart_LuaAstart_constructor'", nullptr);
            return 0;
        }
        cobj = new astar::LuaAstart();
        cobj->autorelease();
        int ID =  (int)cobj->_ID ;
        int* luaID =  &cobj->_luaID ;
        toluafix_pushusertype_ccobject(tolua_S, ID, luaID, (void*)cobj,"as.LuaAstart");
        return 1;
    }
    luaL_error(tolua_S, "%s has wrong number of arguments: %d, was expecting %d \n", "as.LuaAstart:LuaAstart",argc, 0);
    return 0;

#if COCOS2D_DEBUG >= 1
    tolua_error(tolua_S,"#ferror in function 'lua_astart_LuaAstart_constructor'.",&tolua_err);
#endif

    return 0;
}

static int lua_astart_LuaAstart_finalize(lua_State* tolua_S)
{
    printf("luabindings: finalizing LUA object (LuaAstart)");
    return 0;
}

int lua_register_astart_LuaAstart(lua_State* tolua_S)
{
    tolua_usertype(tolua_S,"as.LuaAstart");
    tolua_cclass(tolua_S,"LuaAstart","as.LuaAstart","cc.Ref",nullptr);

    tolua_beginmodule(tolua_S,"LuaAstart");
        tolua_function(tolua_S,"new",lua_astart_LuaAstart_constructor);
        tolua_function(tolua_S,"setbarrier",lua_astart_LuaAstart_setbarrier);
        tolua_function(tolua_S,"setPathList",lua_astart_LuaAstart_setPathList);
        tolua_function(tolua_S,"init",lua_astart_LuaAstart_init);
        tolua_function(tolua_S,"caclPath",lua_astart_LuaAstart_caclPath);
        tolua_function(tolua_S,"getPathList",lua_astart_LuaAstart_getPathList);
        tolua_function(tolua_S,"setspPostion",lua_astart_LuaAstart_setspPostion);
        tolua_function(tolua_S,"getspPostion",lua_astart_LuaAstart_getspPostion);
        tolua_function(tolua_S,"create", lua_astart_LuaAstart_create);
    tolua_endmodule(tolua_S);
    std::string typeName = typeid(astar::LuaAstart).name();
    g_luaType[typeName] = "as.LuaAstart";
    g_typeCast["LuaAstart"] = "as.LuaAstart";
    return 1;
}
TOLUA_API int register_all_astart(lua_State* tolua_S)
{
	tolua_open(tolua_S);
	
	tolua_module(tolua_S,"as",0);
	tolua_beginmodule(tolua_S,"as");

	lua_register_astart_MyVec2(tolua_S);
	lua_register_astart_LuaAstart(tolua_S);

	tolua_endmodule(tolua_S);
	return 1;
}

